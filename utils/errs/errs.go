package errors

import (
	"bloc/utils"

	"github.com/gofiber/fiber/v2"
	"github.com/rs/zerolog/log"
)

func Handle(c *fiber.Ctx, args ...interface{}) error {
	var res utils.Reponse

	if args[0] == nil {
		log.Panic().Msg("The response must be specified !!")
	}

	res = utils.Reponse{
		Status:  args[0].(utils.Reponse).Status,
		Message: args[0].(utils.Reponse).Message,
		Code:    args[0].(utils.Reponse).Code,
	}

	if len(args) > 1 {
		switch args[1].(type) {
		case error:
			log.Err(args[1].(error)).Msg(args[1].(error).Error()) // Print error
			res.Content = args[1].(error).Error()
		default:
			res.Content = args[1]
		}
	}

	return c.Status(res.Code).JSON(res)
}

var (
	/* INTERNAL */
	Success = utils.Reponse{
		Code:    200,
		Status:  "SUCCESS",
		Message: "Request treated with success!",
	}
	ErrRequest = utils.Reponse{
		Code:    400,
		Status:  "ERROR_REQUEST",
		Message: "The sever can't handle the request",
	}
	ErrBody = utils.Reponse{
		Code:    400,
		Status:  "ERROR_BROKEN_BODY",
		Message: "Error while parsing body",
	}
	ErrUnknown = utils.Reponse{
		Code:    500,
		Status:  "ERROR_UNKNOWN",
		Message: "An unknown error occured",
	}
	ErrPermission = utils.Reponse{
		Code:    403,
		Status:  "ERROR_PERMISSION",
		Message: "You can't access this content",
	}
	ErrBase64Decode = utils.Reponse{
		Code:    500,
		Status:  "ERROR_BASE64",
		Message: "Can't parse base64 string",
	}

	/* DATABASE */
	ErrDatabaseGeneric = utils.Reponse{
		Code:    500,
		Status:  "ERROR_DB",
		Message: "An error occured during database request",
	}
	ErrDatabaseCreate = utils.Reponse{
		Code:    500,
		Status:  "ERROR_DB",
		Message: "Failed to put data in the database",
	}
	ErrDatabaseUpdate = utils.Reponse{
		Code:    500,
		Status:  "ERROR_DB",
		Message: "Failed to update data in the database",
	}
	ErrDatabaseRemove = utils.Reponse{
		Code:    500,
		Status:  "ERROR_DB",
		Message: "Failed to remove data from the database",
	}
	ErrDatabaseNotFound = utils.Reponse{
		Code:    500,
		Status:  "ERROR_DB",
		Message: "Failed to find data in the database",
	}

	/* AUTH */
	ErrAuth = utils.Reponse{
		Code:    400,
		Status:  "ERROR_AUTH",
		Message: "An unknown error occured during authentication",
	}
	ErrAuthExist = utils.Reponse{
		Code:    400,
		Status:  "AUTH_ALREADY_EXIST",
		Message: "This user already exist",
	}
	ErrAuthNotFound = utils.Reponse{
		Code:    400,
		Status:  "AUTH_USER_NOT_FOUND",
		Message: "User not found",
	}
	ErrAuthChallengeFailed = utils.Reponse{
		Code:    400,
		Status:  "AUTH_CHALLENGE_FAILDED",
		Message: "The verification of the signature has failed",
	}
	ErrAuthReplayAttack = utils.Reponse{
		Code:    400,
		Status:  "AUTH_REPLAY_ATTACK_DETECTED",
		Message: "The request you are sending has already been sent before",
	}
	ErrAuthInvalidUsername = utils.Reponse{
		Code:    400,
		Status:  "AUTH_INVALID_USERNAME",
		Message: "Username have invalid characters or is smaller than 3 characters",
	}

	/* OAUTH */
	ErrOAuthInvalidToken = utils.Reponse{
		Code:    400,
		Status:  "OAUTH_INVALID_TOKEN",
		Message: "Issued token seems invalid",
	}
	ErrOAuthEnable = utils.Reponse{
		Code:    400,
		Status:  "OAUTH_ENABLED",
		Message: "Local authentification is disabled, please use oauth",
	}
)
