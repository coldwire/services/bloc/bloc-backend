package controller

import (
	"bloc/controller/auth"
	"bloc/database"
	"bloc/storage"
	"bloc/utils/config"
	"bloc/utils/env"
	"bloc/utils/tokens"
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http/httptest"
	"testing"

	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert" // add Testify package
	"gitlab.com/coldwire/libraries/liboxyd/liboxyd-go"
)

func generateBody(body interface{}) *bytes.Buffer {
	res, _ := json.Marshal(body)

	fmt.Println("Request body:", string(res))

	return bytes.NewBuffer(res)
}

func TestAuth(t *testing.T) {
	// Test datas
	var TEST_USER_SECRET_KEY, TEST_USER_PUBLIC_KEY = liboxyd.Secp256k1_generate_keypair()
	var TEST_AUTH_CHALLENGE = liboxyd.Base64_encode(uuid.NewString())
	var TEST_USER_NAME = "meowemow"

	// Init server
	config.Init("../config.toml")
	database.Connect()
	storage.Init(config.Conf.Storage.Driver)
	tokens.Init(env.Get("JWT_KEY", "hello"))
	app := fiber.New()

	// Create test routes

	/* ------------------ REGISTER ------------------ */
	app.Post("/register", auth.Register)

	registerRequest := httptest.NewRequest("POST", "/register", generateBody(auth.RegisterRequest{
		Username:  TEST_USER_NAME,
		PublicKey: TEST_USER_PUBLIC_KEY,
	}))
	registerRequest.Header.Set("Content-Type", "application/json")

	registerResponse, _ := app.Test(registerRequest, 1000)
	defer registerResponse.Body.Close()

	registerBody, _ := ioutil.ReadAll(registerResponse.Body)
	fmt.Println("response Body:", string(registerBody))

	// Verify, if the status code is as expected
	assert.Equalf(t, 200, registerResponse.StatusCode, "Register new user")

	/* ------------------ LOGIN ------------------ */
	app.Post("/login", auth.Login)

	signrature, err := liboxyd.Secp256k1_sign(TEST_USER_SECRET_KEY, TEST_AUTH_CHALLENGE)
	if err != nil {
		log.Println(err)
	}

	loginRequest := httptest.NewRequest("POST", "/login", generateBody(auth.LoginRequest{
		Username:        TEST_USER_NAME,
		Challenge:       TEST_AUTH_CHALLENGE,
		SignedChallenge: signrature,
	}))

	loginRequest.Header.Set("Content-Type", "application/json")

	loginResponse, _ := app.Test(loginRequest, 1000)
	defer loginResponse.Body.Close()

	loginBody, _ := ioutil.ReadAll(loginResponse.Body)
	fmt.Println("response Body:", string(loginBody))

	// Verify, if the status code is as expected
	assert.Equalf(t, 200, loginResponse.StatusCode, "Login user with signed challenge")
}
