package blocs

import (
	"bloc/models"
	errors "bloc/utils/errs"
	"bloc/utils/tokens"

	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
)

// Folders bloc type related functions

type CreateRequest struct {
	Name   string `json:"name"`
	Parent string `json:"parent"`
}

type CreateResponse struct {
	Id     string `json:"id"`
	Name   string `json:"name"`
	Parent string `json:"parent"`
	Owner  string `json:"owner"`
}

// Create godoc
// @Summary      Create a new bloc folder
// @Tags         Blocs
// @Accept		 json
// @Produce      json
// @Param        Content  body  CreateRequest  true  "Create request"
// @Success      200   		   {object}  CreateResponse
// @Router       /blocs/create [POST]
func Create(c *fiber.Ctx) error {
	var request CreateRequest

	err := c.BodyParser(&request)
	if err != nil {
		return errors.Handle(c, errors.ErrBody, err)
	}

	token, err := tokens.Parse(c.Cookies("token")) // Parse user's JWT token
	if err != nil {
		return errors.Handle(c, errors.ErrAuth, err)
	}

	parent := models.Bloc{Id: request.Parent}
	parent, err = parent.Find()
	if err != nil {
		return errors.Handle(c, errors.ErrDatabaseNotFound, err)
	}

	// if the parent dir is not owned by the user, cancel upload
	if parent.Owner != token.Username {
		return errors.Handle(c, errors.ErrPermission)
	}

	// Create bloc
	var bloc = models.Bloc{
		Id:       uuid.NewString(),
		Name:     request.Name,
		Owner:    token.Username,
		Parent:   request.Parent,
		BlocType: "FOLDER",
	}

	err = bloc.Create()
	if err != nil {
		return errors.Handle(c, errors.ErrDatabaseCreate, err)
	}

	err = bloc.SetOwner(token.Username)
	if err != nil {
		return errors.Handle(c, errors.ErrDatabaseUpdate, err)
	}

	err = bloc.SetParent(request.Parent)
	if err != nil {
		return errors.Handle(c, errors.ErrDatabaseUpdate, err)
	}

	return errors.Handle(c, errors.Success, CreateResponse{
		Id:     bloc.Id,
		Name:   bloc.Name,
		Parent: bloc.Parent,
		Owner:  bloc.Owner,
	})
}
