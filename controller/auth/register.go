package auth

import (
	"bloc/models"
	"bloc/utils"
	errors "bloc/utils/errs"
	"bloc/utils/tokens"
	"regexp"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
)

type RegisterRequest struct {
	Username  string `json:"username"`
	PublicKey string `json:"public_key"`
}

type RegisterResponse struct {
	Username        string `json:"username"`
	TokenExpiration int64  `json:"token_expiration"`
	PublicKey       string `json:"public_key"`
	Root            string `json:"root"`
}

// Register godoc
// @Summary      Create a new user account with username and password
// @Tags         Auth
// @Accept       json
// @Produce      json
// @Param        Content  body  RegisterRequest  true  "register"
// @Success      200  {object}  RegisterResponse
// @Router       /auth/register [POST]
func Register(c *fiber.Ctx) error {
	var request = RegisterRequest{}

	err := c.BodyParser(&request)
	if err != nil {
		return errors.Handle(c, errors.ErrBody, err)
	}

	var root = models.Bloc{
		Id:       uuid.NewString(),
		Name:     "root",
		BlocType: "FOLDER",
	}

	var usr = models.User{
		Username:  request.Username,
		PublicKey: request.PublicKey,
		AuthMode:  "LOCAL",
	}

	usernameValidation, err := regexp.MatchString("[a-zA-Z]{3,}", request.Username)
	if !usernameValidation {
		return errors.Handle(c, errors.ErrBody, err)
	}

	exist, err := usr.Exist()
	if !exist && err != nil {
		return errors.Handle(c, errors.ErrAuth, err)
	}

	if exist {
		return errors.Handle(c, errors.ErrAuthExist)
	}

	// Create root folder
	err = root.Create()
	if err != nil {
		return errors.Handle(c, errors.ErrDatabaseCreate, err)
	}

	usr.Root = root.Id // add root folder to the user for the response

	err = usr.Create()
	if err != nil {
		return errors.Handle(c, errors.ErrDatabaseCreate, err)
	}

	err = usr.SetRoot(root.Id) // Set root folder of the user
	if err != nil {
		return errors.Handle(c, errors.ErrDatabaseCreate, err)
	}

	err = root.SetOwner(usr.Username) // Set the owner of the root folder
	if err != nil {
		return errors.Handle(c, errors.ErrDatabaseCreate, err)
	}

	expiration := time.Hour * 12

	token, tokenHeader := tokens.Generate(tokens.Token{
		Username: request.Username,
	}, expiration)

	utils.SetCookie(c, "token", token, time.Now().Add(expiration))

	return errors.Handle(c, errors.Success, RegisterResponse{
		Username:        usr.Username,
		TokenExpiration: tokenHeader.Expiry,
		PublicKey:       usr.PublicKey,
		Root:            usr.Root,
	})
}
