package models

import (
	"bloc/database"
	"context"
	"time"

	"github.com/georgysavva/scany/pgxscan"
	"github.com/rs/zerolog/log"
)

type Bloc struct {
	Id     string `db:"id"            json:"id"`
	Name   string `db:"name"          json:"name"`
	Owner  string `db:"owner"         json:"owner"`
	Parent string `db:"parent"        json:"parent"`

	BlocType     string `db:"bloc_type"     json:"bloc_type"`
	BlocProperty string `db:"bloc_property" json:"bloc_property"`

	CreatedOn time.Time `db:"created_on" json:"created_on"`
	EditedOn  time.Time `db:"edited_on"  json:"edited_on"`
}

func (b Bloc) Create() error {
	_, err := database.DB.Exec(context.Background(), `INSERT INTO blocs(id, name, bloc_type) VALUES ($1, $2, $3)`, b.Id, b.Name, b.BlocType)
	if err != nil {
		log.Err(err).Msg(err.Error())
		return err
	}

	return nil
}

func (b Bloc) Delete() error {
	_, err := database.DB.Exec(context.Background(), `DELETE FROM blocs WHERE id = $1`, b.Id)
	if err != nil {
		log.Err(err).Msg(err.Error())
		return err
	}

	return err
}

func (b Bloc) Find() (Bloc, error) {
	var bloc Bloc
	err := pgxscan.Get(context.Background(), database.DB, &bloc, `
	SELECT
		id,
		name,
		coalesce(f_owner, '') AS owner,
		coalesce(f_parent, '') AS parent,
		bloc_type,
		coalesce(f_bloc_property, '') AS bloc_property,
		created_on,
		edited_on
			FROM blocs
				WHERE id = $1`, b.Id)

	if err != nil {
		log.Err(err).Msg(err.Error())
		return bloc, err
	}

	return bloc, nil
}

func (b Bloc) List() ([]Bloc, error) {
	var (
		blocs []Bloc
		err   error
	)

	blocsRows, err := database.DB.Query(context.Background(), `SELECT
	id,
	name,
	coalesce(f_owner, '') AS owner,
	coalesce(f_parent, '') AS parent,
	bloc_type,
	coalesce(f_bloc_property, '') AS bloc_property,
	created_on,
	edited_on
		FROM blocs
			WHERE f_parent = $1`, b.Parent)
	if err != nil {
		log.Err(err).Msg(err.Error())
		return blocs, err
	}

	err = pgxscan.ScanAll(&blocs, blocsRows)
	if err != nil {
		log.Err(err).Msg(err.Error())
		return blocs, err
	}

	return blocs, err
}

func (b Bloc) SetOwner(username string) error {
	_, err := database.DB.Exec(context.Background(), `UPDATE blocs SET f_owner = $1 WHERE id = $2`, username, b.Id)
	if err != nil {
		log.Err(err).Msg(err.Error())
		return err
	}

	return err
}

func (b Bloc) SetProperty(id string) error {
	_, err := database.DB.Exec(context.Background(), `UPDATE blocs SET f_bloc_property = $1 WHERE id = $2`, id, b.Id)
	if err != nil {
		log.Err(err).Msg(err.Error())
		return err
	}

	return err
}

func (b Bloc) SetParent(id string) error {
	_, err := database.DB.Exec(context.Background(), `UPDATE blocs SET f_parent = $1 WHERE id = $2`, id, b.Id)
	if err != nil {
		log.Err(err).Msg(err.Error())
		return err
	}

	return err
}

func (b Bloc) SetEditedOn(time int64) error {
	_, err := database.DB.Exec(context.Background(), `UPDATE blocs SET edited_on = $1 WHERE id = $2`, time, b.Id)
	if err != nil {
		log.Err(err).Msg(err.Error())
		return err
	}

	return err
}
