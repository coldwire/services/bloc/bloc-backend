package auth

import (
	"bloc/models"
	"bloc/utils"
	errors "bloc/utils/errs"
	"bloc/utils/tokens"
	"regexp"
	"time"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/coldwire/libraries/liboxyd/liboxyd-go"
)

type LoginRequest struct {
	Username        string `json:"username"`
	Challenge       string `json:"challenge"`
	SignedChallenge string `json:"signed_challenge"`
}

type LoginResponse struct {
	Username        string `json:"username"`
	TokenExpiration int64  `json:"token_expiration"`
	PublicKey       string `json:"public_key"`
	Root            string `json:"root"`
}

// Login godoc
// @Summary      Create a new session for a user
// @Tags         Auth
// @Accept       json
// @Produce      json
// @Param        Content  body  LoginRequest  true  "Login request"
// @Success      200  {object}  LoginResponse
// @Router       /auth/login [POST]
func Login(c *fiber.Ctx) error {
	var request = LoginRequest{}

	err := c.BodyParser(&request)
	if err != nil {
		return errors.Handle(c, errors.ErrBody)
	}

	var user = models.User{
		Username: request.Username,
	}

	usernameValidation, _ := regexp.MatchString("[a-zA-Z]{3,}", request.Username)
	if !usernameValidation {
		return errors.Handle(c, errors.ErrAuthInvalidUsername)
	}

	user, err = user.Find()
	if err != nil {
		return errors.Handle(c, errors.ErrAuthNotFound, err)
	}

	isValid, res, err := KeyAuth(request.Username, user.PublicKey, request.Challenge, request.SignedChallenge)
	if !isValid {
		return errors.Handle(c, res, err)
	}

	expiration := time.Hour * 12

	token, tokenHeader := tokens.Generate(tokens.Token{
		Username: request.Username,
	}, expiration)

	utils.SetCookie(c, "token", token, time.Now().Add(expiration))

	return errors.Handle(c, errors.Success, LoginResponse{
		Username:        user.Username,
		TokenExpiration: tokenHeader.Expiry,
		PublicKey:       user.PublicKey,
		Root:            user.Root,
	})
}

// This function is a helper for cryptographic signature challenge auth based
func KeyAuth(username string, publicKey string, challenge string, signedChallenge string) (bool, utils.Reponse, error) {
	challengeModel := models.Challenge{
		Id:        challenge,
		Signature: signedChallenge,
	}

	isReplay, err := challengeModel.Exist()
	if !isReplay && err != nil {
		return false, errors.ErrAuth, err
	}

	if isReplay {
		return false, errors.ErrAuthReplayAttack, nil
	}

	isValid, err := liboxyd.Secp256k1_verify(publicKey, signedChallenge, challenge)
	if err != nil {
		return false, errors.ErrAuth, err
	}

	if !isValid {
		return false, errors.ErrAuthChallengeFailed, nil
	}

	err = challengeModel.Create()
	if err != nil {
		return false, errors.ErrDatabaseCreate, err
	}

	return true, errors.Success, nil
}
