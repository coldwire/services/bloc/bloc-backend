package models

import (
	"bloc/database"
	"context"
	"log"

	"github.com/georgysavva/scany/pgxscan"
)

type User struct {
	Username  string `db:"username"      json:"username"`
	AuthMode  string `db:"auth_mode"     json:"authMode"`
	PublicKey string `db:"public_key"    json:"publicKey"`
	Root      string `db:"root"          json:"root"`
}

func (u User) Create() error {
	_, err := database.DB.Exec(context.Background(), `INSERT INTO users(username, auth_mode, public_key) VALUES($1, $2, $3)`, u.Username, u.AuthMode, u.PublicKey)
	if err != nil {
		log.Println(err.Error())
		return err
	}

	return nil
}

func (u User) Delete() error {
	_, err := database.DB.Exec(context.Background(), `DELETE FROM users WHERE username = $1`, u.Username)
	if err != nil {
		log.Println(err.Error())
		return err
	}

	return err
}

func (u User) Find() (User, error) {
	var user User
	err := pgxscan.Get(context.Background(), database.DB, &user, `SELECT
	username,
	auth_mode,
	f_root AS root,
	public_key
		FROM users
			WHERE username = $1`, u.Username)

	if err != nil {
		log.Println(err.Error())
		return user, err
	}

	return user, err
}

func (u User) GetPublicKey() (string, error) {
	var publicKey string
	err := pgxscan.Get(context.Background(), database.DB, &publicKey, `SELECT
	public_key
		FROM users
			WHERE username = $1`, u.Username)

	if err != nil {
		log.Println(err.Error())
		return publicKey, err
	}

	return publicKey, err
}

func (u User) Exist() (bool, error) {
	var user User
	err := pgxscan.Get(context.Background(), database.DB, &user, `SELECT
	username
		FROM users
			WHERE username = $1`, u.Username)

	if pgxscan.NotFound(err) {
		return false, nil
	}

	if err != nil {
		return false, err
	}

	return true, err
}

func (u User) SetRoot(id string) error {
	_, err := database.DB.Exec(context.Background(), `UPDATE users SET f_root = $1 WHERE username = $2`, id, u.Username)
	if err != nil {
		log.Println(err.Error())
		return err
	}

	return err
}
