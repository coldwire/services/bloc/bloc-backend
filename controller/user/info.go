package user

import (
	"bloc/models"
	errors "bloc/utils/errs"
	"bloc/utils/tokens"

	"github.com/gofiber/fiber/v2"
)

type UserResponse struct {
	Username        string `json:"username"`
	TokenExpiration int64  `json:"token_expiration"`
	PublicKey       string `json:"public_key"`
	PrivateKey      string `json:"private_key"`
	Root            string `json:"root"`
}

func Info(c *fiber.Ctx) error {
	userToken := c.Cookies("token")

	token, err := tokens.Parse(userToken)
	if err != nil {
		return errors.Handle(c, errors.ErrAuth, err)
	}

	usr := models.User{
		Username: token.Username,
	}

	user, err := usr.Find()
	if err != nil {
		return errors.Handle(c, errors.ErrDatabaseGeneric)
	}

	return errors.Handle(c, errors.Success, UserResponse{
		Username:        user.Username,
		TokenExpiration: token.Expiry,
		PublicKey:       user.PublicKey,
		Root:            user.Root,
	})
}
