package routes

import (
	"bloc/controller/auth"
	"bloc/middlewares"

	"github.com/gofiber/fiber/v2"
)

func setupAuthRoutes(api fiber.Router) {
	// Auth > /api/auth
	authRoute := api.Group("/auth")

	authRoute.Post("/register", middlewares.IsOauthEnabled, auth.Register)
	authRoute.Post("/login", middlewares.IsOauthEnabled, auth.Login)
	authRoute.Get("/logout", auth.Logout)

	// Oauth2 > /api/auth/oauth2
	oauthRoute := authRoute.Group("/oauth2")
	oauthRoute.Get("/", auth.Oauth2)
	oauthRoute.Get("/callback", auth.Oauth2Callback)

}
