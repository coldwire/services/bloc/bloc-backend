package models

import (
	"bloc/database"
	"context"
	"log"

	"github.com/georgysavva/scany/pgxscan"
)

type Challenge struct {
	Id        string `db:"id"        json:"id"`
	Signature string `db:"signature" json:"signature"`
}

func (c Challenge) Create() error {
	_, err := database.DB.Exec(context.Background(), `INSERT INTO challenges(id, signature) VALUES($1, $2)`, c.Id, c.Signature)
	if err != nil {
		log.Println(err.Error())
		return err
	}

	return err
}

func (c Challenge) Exist() (bool, error) {
	var challenge Challenge
	err := pgxscan.Get(context.Background(), database.DB, &challenge, `SELECT
		id,
		signature
		FROM challenges
			WHERE id = $1`, c.Id)

	if pgxscan.NotFound(err) {
		return false, nil
	}

	if err != nil {
		return false, err
	}

	return true, err
}
