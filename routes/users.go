package routes

import (
	"bloc/controller/user"
	"bloc/middlewares"

	"github.com/gofiber/fiber/v2"
)

func setupUsersRoutes(api fiber.Router) {
	// User > /api/user/
	userRoute := api.Group("/user", middlewares.IsAuthenticated)
	userRoute.Get("/info", user.Info)
}
