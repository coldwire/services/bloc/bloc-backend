package blocs

import (
	"bloc/models"
	"bloc/storage"
	errors "bloc/utils/errs"
	"bloc/utils/tokens"

	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
)

// Files bloc type related functions

type UploadResponse struct {
	Id     string `json:"id"`
	Name   string `json:"name"`
	Parent string `json:"parent"`
	Owner  string `json:"owner"`

	Size       int    `json:"size"`
	MimeType   string `json:"mime_type"`
	IsFavorite bool   `json:"is_favorite"`
}

// Upload godoc
// @Summary      Upload
// @Description  Upload a new file and store it as a bloc
// @Tags         Blocs
// @Accept       mpfd
// @Produce      json
// @Param        file     formData  file     true  "File to upload"
// @Param        key      formData  string   true  "Encrypted encryption key of the file"
// @Param        parent   formData  string   true  "Parent bloc of this bloc"
// @Success      200   		   {object}  UploadResponse
// @Router       /blocs/upload [post]
func Upload(c *fiber.Ctx) error {
	fileMultipart, err := c.FormFile("file") // We are getting the file sent via a form
	if err != nil {
		return errors.Handle(c, errors.ErrRequest, err)
	}

	// Get parent folder id
	parent := c.FormValue("parent")

	// Get encrypted key to store it, so the user never lost it
	// TODO: Add a regex to verify that a key is specified
	key := c.FormValue("key")

	if key == "" || parent == "" {
		return errors.Handle(c, errors.ErrRequest, "please specify a key and a parent")
	}

	if fileMultipart.Header["Content-Type"] == nil {
		return errors.Handle(c, errors.ErrRequest, "no mime-header specified")
	}

	token, err := tokens.Parse(c.Cookies("token")) // Parse user's JWT token
	if err != nil {
		return errors.Handle(c, errors.ErrAuth, err)
	}

	p := models.Bloc{Id: parent}
	p, err = p.Find()
	if err != nil {
		return errors.Handle(c, errors.ErrDatabaseNotFound, err)
	}

	// if the parent dir is not owned by the user, cancel upload
	if p.Owner != token.Username {
		return errors.Handle(c, errors.ErrPermission)
	}

	// TODO: check user's quota before upload

	// Create bloc
	var bloc = models.Bloc{
		Id:       uuid.NewString(),
		Name:     fileMultipart.Filename,
		Owner:    token.Username,
		Parent:   parent,
		BlocType: "FILE",
	}

	// Create files properties
	var properties = models.BlocProperties{
		Id:         uuid.NewString(),
		Size:       int(fileMultipart.Size),
		MimeType:   fileMultipart.Header["Content-Type"][0],
		IsFavorite: false,
		Key:        key,
	}

	// Upload file using the configured storage driver
	err = storage.Driver.Create(bloc.Id, fileMultipart)
	if err != nil {
		return errors.Handle(c, errors.ErrUnknown, err)
	}

	err = bloc.Create()
	if err != nil {
		storage.Driver.Delete(bloc.Id)
		return errors.Handle(c, errors.ErrDatabaseCreate, err)
	}

	err = properties.Create()
	if err != nil {
		return errors.Handle(c, errors.ErrDatabaseCreate, err)
	}

	err = bloc.SetOwner(token.Username)
	if err != nil {
		return errors.Handle(c, errors.ErrDatabaseUpdate, err)
	}

	err = bloc.SetProperty(properties.Id)
	if err != nil {
		return errors.Handle(c, errors.ErrDatabaseUpdate, err)
	}

	err = bloc.SetParent(parent)
	if err != nil {
		return errors.Handle(c, errors.ErrDatabaseUpdate, err)
	}

	return errors.Handle(c, errors.Success, UploadResponse{
		Id:     bloc.Id,
		Name:   bloc.Name,
		Parent: bloc.Parent,
		Owner:  bloc.Owner,

		Size:       properties.Size,
		MimeType:   properties.MimeType,
		IsFavorite: properties.IsFavorite,
	})
}

// Download godoc
// @Summary      Download a file
// @Tags         Blocs
// @Accept       json
// @Produce      mpfd
// @Param        id  path  string  true  "Id of the bloc to download"
// @Success      200
// @Router       /blocs/download/{id} [get]
func Download(c *fiber.Ctx) error {
	blocId := c.Params("id")

	bloc := models.Bloc{
		Id: blocId,
	}

	token, err := tokens.Parse(c.Cookies("token")) // Parse user's JWT token
	if err != nil {
		return errors.Handle(c, errors.ErrAuth, err)
	}

	// Get bloc
	bloc, err = bloc.Find()
	if err != nil {
		return errors.Handle(c, errors.ErrDatabaseNotFound, err)
	}

	// Check if this bloc belong to the user making the request
	if token.Username != bloc.Owner {
		return errors.Handle(c, errors.ErrPermission)
	}

	// Check if this bloc is a file
	if bloc.BlocType == "FOLDER" {
		return errors.Handle(c, errors.ErrRequest, "you can't download folders")
	}

	// Get properties of the bloc
	properties := models.BlocProperties{
		Id: bloc.BlocProperty,
	}

	properties, err = properties.Find()
	if err != nil {
		return errors.Handle(c, errors.ErrDatabaseNotFound, err)
	}

	c.Response().Header.Add("Content-Disposition", "attachment; filename=\""+bloc.Name+"\"")
	c.Response().Header.Add("Content-Type", properties.MimeType)

	stream, err := storage.Driver.Get(blocId)
	if err != nil {
		return errors.Handle(c, errors.ErrUnknown, err)
	}

	return c.SendStream(stream)
}
