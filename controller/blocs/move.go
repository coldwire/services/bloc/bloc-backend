package blocs

import (
	"bloc/models"
	errors "bloc/utils/errs"
	"bloc/utils/tokens"

	"github.com/gofiber/fiber/v2"
)

// Move godoc
// @Summary      Change parent of a bloc
// @Tags         Blocs
// @Accept       json
// @Produce      json
// @Param        id   path  string  true  "Id of the bloc to move"
// @Param        new  path  string  true  "Id of the new parent"
// @Success      200
// @Router       /blocs/move/{id}/{new} [PUT]
func Move(c *fiber.Ctx) error {
	id := c.Params("id")   // Id of the bloc to move
	new := c.Params("new") // Id of the new parent

	token, err := tokens.Parse(c.Cookies("token")) // Parse user's JWT token
	if err != nil {
		return errors.Handle(c, errors.ErrAuth, err)
	}

	newParent := models.Bloc{
		Id: new,
	}

	newParent, err = newParent.Find()
	if err != nil {
		return errors.Handle(c, errors.ErrDatabaseNotFound, err)
	}

	if newParent.BlocType == "FILE" {
		return errors.Handle(c, errors.ErrRequest, "You can't use a file as a parent")
	}

	// Here we check if the new parent really belong to user
	if newParent.Owner != token.Username {
		return errors.Handle(c, errors.ErrPermission, err)
	}

	bloc := models.Bloc{
		Id: id,
	}

	err = bloc.SetParent(new)
	if err != nil {
		return errors.Handle(c, errors.ErrDatabaseUpdate, "failde to update parent folder")
	}

	return errors.Handle(c, errors.Success)
}
