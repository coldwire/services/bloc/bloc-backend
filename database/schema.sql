DO $$ BEGIN
    CREATE TYPE authmode AS ENUM ('LOCAL', 'OAUTH2');
EXCEPTION
    WHEN duplicate_object THEN null;
END $$;

DO $$ BEGIN
    CREATE TYPE bloctype AS ENUM ('FILE', 'FOLDER');
EXCEPTION
    WHEN duplicate_object THEN null;
END $$;

CREATE TABLE IF NOT EXISTS challenges (
  id           VARCHAR(256) PRIMARY KEY NOT NULL UNIQUE,
  signature    VARCHAR(512) NOT NULL
);

CREATE TABLE IF NOT EXISTS users (
  username      VARCHAR(24) PRIMARY KEY NOT NULL UNIQUE,
  auth_mode     authmode NOT NULL DEFAULT 'LOCAL',
  f_root        VARCHAR(256) DEFAULT NULL,
  public_key    VARCHAR(256) NOT NULL
);

CREATE TABLE IF NOT EXISTS bloc_properties (
  id          VARCHAR(256) PRIMARY KEY NOT NULL UNIQUE,
  size        BIGINT NOT NULL,
  mime_type   VARCHAR(256) NOT NULL,
  is_favorite BOOLEAN NOT NULL DEFAULT FALSE,
  key         VARCHAR(256) NOT NULL
);

CREATE TABLE IF NOT EXISTS blocs (
  id              VARCHAR(256) PRIMARY KEY NOT NULL UNIQUE,
  name            VARCHAR(128) NOT NULL,
  bloc_type       bloctype NOT NULL DEFAULT 'FOLDER',
  f_owner         VARCHAR(24) DEFAULT NULL,
  f_parent        VARCHAR(256) DEFAULT NULL,
  f_bloc_property VARCHAR(256) DEFAULT NULL,
  created_on      TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT (NOW() AT TIME ZONE 'utc'),
  edited_on       TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT (NOW() AT TIME ZONE 'utc'),

  FOREIGN KEY (f_bloc_property)
    REFERENCES bloc_properties(id)
      ON DELETE CASCADE,

  FOREIGN KEY (f_parent)
    REFERENCES blocs(id)
      ON DELETE CASCADE,

  FOREIGN KEY (f_owner)
    REFERENCES users(username)
      ON DELETE CASCADE
);

ALTER TABLE users ADD
  FOREIGN KEY (f_root)
    REFERENCES blocs(id); 


CREATE TABLE IF NOT EXISTS shares (
  id          VARCHAR(256) PRIMARY KEY NOT NULL UNIQUE,
  key         VARCHAR(256) NOT NULL,
  is_favorite BOOLEAN NOT NULL DEFAULT FALSE,
  bloc_type   bloctype NOT NULL DEFAULT 'FOLDER',
  f_bloc      VARCHAR(256) DEFAULT NULL,
  f_owner     VARCHAR(24) NOT NULL,
  f_parent    VARCHAR(256) DEFAULT NULL,

  CONSTRAINT c_bloc
    FOREIGN KEY (f_bloc)
      REFERENCES blocs(id)
        ON DELETE CASCADE,

  CONSTRAINT c_parent
    FOREIGN KEY (f_parent)
      REFERENCES blocs(id)
        ON DELETE CASCADE,

  CONSTRAINT c_owner
    FOREIGN KEY (f_owner)
      REFERENCES users(username)
);